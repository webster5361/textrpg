insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (1,'0,0',0,0,'0,0','','','','S(7),E(2)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (2,'1,0',1,0,'1,0','','','','W(1),E(3)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (3,'2,0',2,0,'2,0','','','','W(2),E(4)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (4,'3,0',3,0,'3,0','','','','W(3),E(5)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (5,'4,0',4,0,'4,0','','','','W(4),E(6)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (6,'5,0',5,0,'5,0','','','','S(8),W(5)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (7,'0,1',0,1,'0,1','','','','N(1),S(9)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (8,'5,1',5,1,'5,1','','','','N(6),S(14)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (9,'0,2',0,2,'0,2','','','','N(7),E(10),S(20)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (10,'1,2',1,2,'1,2','','','','W(9),E(11)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (11,'2,2',2,2,'2,2','','','','W(10),E(12),S(21)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (12,'3,2',3,2,'3,2','','','','W(11),E(13),S(22)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (13,'4,2',4,2,'4,2','','','','W(12),E(14),S(23)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (14,'5,2',5,2,'5,2','','','','N(8),W(13),E(15),S(24)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (15,'6,2',6,2,'6,2','','','','W(14),E(16),S(25)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (16,'7,2',7,2,'7,2','','','','W(15),E(17),S(26)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (17,'8,2',8,2,'8,2','','','','W(16),E(18),S(27)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (18,'9,2',9,2,'9,2','','','','W(17),E(19),S(28)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (19,'10,2',10,2,'10,2','','','','W(18),S(29)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (20,'0,3',0,3,'0,3','','','','N(9),S(34)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (21,'2,3',2,3,'2,3','','','','N(11),E(22),S(35)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (22,'3,3',3,3,'3,3','','','','N(12),W(21),E(23),S(36)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (23,'4,3',4,3,'4,3','','','','N(13),W(22),E(24),S(37)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (24,'5,3',5,3,'5,3','','','','N(14),W(23),E(25),S(38)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (25,'6,3',6,3,'6,3','','','','N(15),W(24),E(26),S(39)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (26,'7,3',7,3,'7,3','','','','N(16),W(25),E(27),S(40)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (27,'8,3',8,3,'8,3','','','','N(17),W(26),E(28),S(41)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (28,'9,3',9,3,'9,3','','','','N(18),W(27),E(29),S(42)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (29,'10,3',10,3,'10,3','','','','N(19),W(28),E(30),S(43)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (30,'11,3',11,3,'11,3','','','','W(29),E(31)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (31,'12,3',12,3,'12,3','','','','W(30),E(32)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (32,'13,3',13,3,'13,3','','','','W(31),E(33),S(44)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (33,'14,3',14,3,'14,3','','','','W(32)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (34,'0,4',0,4,'0,4','','','','N(20),S(45)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (35,'2,4',2,4,'2,4','','','','N(21),E(36),S(46)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (36,'3,4',3,4,'3,4','','','','N(22),W(35),E(37),S(47)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (37,'4,4',4,4,'4,4','','','','N(23),W(36),E(38),S(48)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (38,'5,4',5,4,'5,4','','','','N(24),W(37),E(39),S(49)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (39,'6,4',6,4,'6,4','','','','N(25),W(38),E(40),S(50)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (40,'7,4',7,4,'7,4','','','','N(26),W(39),E(41),S(51)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (41,'8,4',8,4,'8,4','','','','N(27),W(40),E(42),S(52)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (42,'9,4',9,4,'9,4','','','','N(28),W(41),E(43),S(53)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (43,'10,4',10,4,'10,4','','','','N(29),W(42),S(54)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (44,'13,4',13,4,'13,4','','','','N(32),S(55)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (45,'0,5',0,5,'0,5','','','','N(34),S(56)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (46,'2,5',2,5,'2,5','','','','N(35),E(47),S(57)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (47,'3,5',3,5,'3,5','','','','N(36),W(46),E(48),S(58)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (48,'4,5',4,5,'4,5','','','','N(37),W(47),E(49),S(59)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (49,'5,5',5,5,'5,5','','','','N(38),W(48),E(50),S(60)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (50,'6,5',6,5,'6,5','','','','N(39),W(49),E(51),S(61)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (51,'7,5',7,5,'7,5','','','','N(40),W(50),E(52),S(62)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (52,'8,5',8,5,'8,5','','','','N(41),W(51),E(53),S(63)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (53,'9,5',9,5,'9,5','','','','N(42),W(52),E(54),S(64)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (54,'10,5',10,5,'10,5','','','','N(43),W(53),S(65)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (55,'13,5',13,5,'13,5','','','','N(44),S(67)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (56,'0,6',0,6,'0,6','','','','N(45),S(68)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (57,'2,6',2,6,'2,6','','','','N(46),E(58),S(69)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (58,'3,6',3,6,'3,6','','','','N(47),W(57),E(59),S(70)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (59,'4,6',4,6,'4,6','','','','N(48),W(58),E(60),S(71)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (60,'5,6',5,6,'5,6','','','','N(49),W(59),E(61),S(72)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (61,'6,6',6,6,'6,6','','','','N(50),W(60),E(62),S(73)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (62,'7,6',7,6,'7,6','','','','N(51),W(61),E(63),S(74)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (63,'8,6',8,6,'8,6','','','','N(52),W(62),E(64),S(75)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (64,'9,6',9,6,'9,6','','','','N(53),W(63),E(65),S(76)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (65,'10,6',10,6,'10,6','','','','N(54),W(64),S(77)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (66,'12,6',12,6,'12,6','','','','E(67)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (67,'13,6',13,6,'13,6','','','','N(55),W(66),S(78)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (68,'0,7',0,7,'0,7','','','','N(56),S(79)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (69,'2,7',2,7,'2,7','','','','N(57),E(70)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (70,'3,7',3,7,'3,7','','','','N(58),W(69),E(71)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (71,'4,7',4,7,'4,7','','','','N(59),W(70),E(72)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (72,'5,7',5,7,'5,7','','','','N(60),W(71),E(73)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (73,'6,7',6,7,'6,7','','','','N(61),W(72),E(74),S(80)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (74,'7,7',7,7,'7,7','','','','N(62),W(73),E(75)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (75,'8,7',8,7,'8,7','','','','N(63),W(74),E(76)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (76,'9,7',9,7,'9,7','','','','N(64),W(75),E(77)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (77,'10,7',10,7,'10,7','','','','N(65),W(76)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (78,'13,7',13,7,'13,7','','','','N(67),S(81)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (79,'0,8',0,8,'0,8','','','','N(68),S(82)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (80,'6,8',6,8,'6,8','','','','N(73),S(83)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (81,'13,8',13,8,'13,8','','','','N(78),S(84)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (82,'0,9',0,9,'0,9','','','','N(79),S(85)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (83,'6,9',6,9,'6,9','','','','N(80),S(89)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (84,'13,9',13,9,'13,9','','','','N(81),S(93)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (85,'0,10',0,10,'0,10','','','','N(82),E(86)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (86,'1,10',1,10,'1,10','','','','W(85),E(87)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (87,'2,10',2,10,'2,10','','','','W(86),S(94)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (88,'5,10',5,10,'5,10','','','','E(89)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (89,'6,10',6,10,'6,10','','','','N(83),S(95),E(90),W(88)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (90,'7,10',7,10,'7,10','','','','W(89)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (91,'11,10',11,10,'11,10','','','','E(92),S(96)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (92,'12,10',12,10,'12,10','','','','W(91),E(93)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (93,'13,10',13,10,'13,10','','','','N(84),W(92)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (94,'2,11',2,11,'2,11','','','','N(87),S(97)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (95,'6,11',6,11,'6,11','','','','N(89)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (96,'11,11',11,11,'11,11','','','','N(91),S(98)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (97,'2,12',2,12,'2,12','','','','N(94),S(99)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (98,'11,12',11,12,'11,12','','','','N(96),S(101)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (99,'2,13',2,13,'2,13','','','','N(97),S(104)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (100,'6,13',6,13,'6,13','','','','S(105)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (101,'11,13',11,13,'11,13','','','','N(98),S(106),E(102)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (102,'12,13',12,13,'12,13','','','','W(101),E(103)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (103,'13,13',13,13,'13,13','','','','W(102),S(107)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (104,'2,14',2,14,'2,14','','','','N(99),S(108)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (105,'6,14',6,14,'6,14','','','','N(100),S(109)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (106,'11,14',11,14,'11,14','','','','N(101),S(110)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (107,'13,14',13,14,'13,14','','','','N(103),S(111)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (108,'2,15',2,15,'2,15','','','','N(104),S(112)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (109,'6,15',6,15,'6,15','','','','N(105),S(116)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (110,'11,15',11,15,'11,15','','','','N(106),S(121)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (111,'13,15',13,15,'13,15','','','','N(107),S(122)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (112,'2,16',2,16,'2,16','','','','N(108),E(113),S(123)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (113,'3,16',3,16,'3,16','','','','W(112),E(115)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (114,'4,16',4,16,'4,16','','','','W(113),E(115)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (115,'5,16',5,16,'5,16','','','','W(114),E(116)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (116,'6,16',6,16,'6,16','','','','N(109),W(115),E(117)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (117,'7,16',7,16,'7,16','','','','W(116),E(118)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (118,'8,16',8,16,'8,16','','','','W(117),E(119),S(124)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (119,'9,16',9,16,'9,16','','','','W(118),E(120)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (120,'10,16',10,16,'10,16','','','','W(119),E(121)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (121,'11,16',11,16,'11,16','','','','N(110),W(120)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (122,'13,16',13,16,'13,16','','','','N(111),S(125)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (123,'2,17',2,17,'2,17','','','','N(112),S(126)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (124,'8,17',8,17,'8,17','','','','N(118),S(127)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (125,'13,17',13,17,'13,17','','','','N(122),S(128)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (126,'2,18',2,18,'2,18','','','','N(123),S(129)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (127,'8,18',8,18,'8,18','','','','N(124),S(130)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (128,'13,18',13,18,'13,18','','','','N(125),S(131)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (129,'2,19',2,19,'2,19','','','','N(126),S(132)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (130,'8,19',8,19,'8,19','','','','N(127),S(133)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (131,'13,19',13,19,'13,19','','','','N(128),S(138)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (132,'2,20',2,20,'2,20','','','','N(129),S(141)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (133,'8,20',8,20,'8,20','','','','N(130),E(134)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (134,'9,20',9,20,'9,20','','','','W(133),E(135)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (135,'10,20',10,20,'10,20','','','','W(134),E(136)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (136,'11,20',11,20,'11,20','','','','W(135),E(137),S(142)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (137,'12,20',12,20,'12,20','','','','W(136),E(138)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (138,'13,20',13,20,'13,20','','','','N(131),W(137)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (139,'0,21',0,21,'0,21','','','','E(140)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (140,'1,21',1,21,'1,21','','','','W(139),E(141)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (141,'2,21',2,21,'2,21','','','','N(132),S(143),W(140)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (142,'11,21',11,21,'11,21','','','','N(136),S(149)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (143,'2,22',2,22,'2,22','','','','N(141),S(150),E(144)',0,'');
insert into tiles(tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (144,'3,22',3,22,'3,22','','','','W(143),E(145)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (145,'4,22',4,22,'4,22','','','','W(144),E(146)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (146,'5,22',5,22,'5,22','','','','W(145),E(147)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (147,'6,22',6,22,'6,22','','','','W(146),E(148)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (148,'7,22',7,22,'7,22','','','','W(147)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (149,'11,22',11,22,'11,22','','','','N(142),S(151)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (150,'2,23',2,23,'2,23','','','','N(143),S(154)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (151,'11,23',11,23,'11,23','','','','N(149),E(152)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (152,'12,23',12,23,'12,23','','','','W(151),E(153)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (153,'13,23',13,23,'13,23','','','','W(152)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (154,'2,24',2,24,'2,24','','','','N(150),S(155)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (155,'2,25',2,25,'2,25','','','','N(154),S(156)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (156,'2,26',2,26,'2,26','','','','N(155),S(157)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (157,'2,27',2,27,'2,27','','','','N(156),S(158)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (158,'2,28',2,28,'2,28','','','','N(157),S(159)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (159,'2,29',2,29,'2,29','','','','N(158),S(160)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (160,'2,30',2,30,'2,30','','','','N(159),S(161)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (161,'2,31',2,31,'2,31','','','','N(160),S(162)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (162,'2,32',2,32,'2,32','','','','N(161),S(163)',0,'');
insert into tiles (tile_id,name,tile_x,tile_y,description,items,enemies,npcs,exits,is_locked,lock_message) VALUES (163,'2,33',2,33,'2,33','','','','N(162),S(163)',0,'');
