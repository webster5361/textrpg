/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author jrwebster
 */
public class Database {
    
    private Connection conn;
    
    public void connect() {
        String url = "jdbc:sqlite:./database/game.db";
        this.conn = null;
        try {
            this.conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established."); 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void selectAllEnemies() {
        String sql = "SELECT * FROM enemies";
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()) {
                System.out.println(rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    public void selectAllWeapons() {
        String sql = "SELECT * FROM weapons";
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()) { 
                System.out.println(rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void selectAllArmor() {
        String sql = "SELECT * FROM armor";
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()) { 
                System.out.println(rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void selectAllTiles() {
        String sql = "SELECT * FROM tiles";
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()) { 
                System.out.println(rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void selectAllItems() {
        String sql = "SELECT * FROM items";
        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()) { 
                System.out.println(rs.getString("name"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
