/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;
import java.util.*;

/**
 *
 * @author jrwebster
 */
public class ScoreKeeper {
    
    private Controller controller; 
    private int player_score;
    private int max_score;
    
    
    public ScoreKeeper(Controller controller) {
        this.setController(controller);
        this.setMax_score(1000);
    }

    /**
     * @return the controller
     */
    public Controller getController() {
        return controller;
    }

    /**
     * @param controller the controller to set
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * @return the player_score
     */
    public int getPlayer_score() {
        return player_score;
    }

    /**
     * @param player_score the player_score to set
     */
    public void setPlayer_score(int player_score) {
        this.player_score = player_score;
    }

    /**
     * @return the max_score
     */
    public int getMax_score() {
        return max_score;
    }

    /**
     * @param max_score the max_score to set
     */
    public void setMax_score(int max_score) {
        this.max_score = max_score;
    }
}
