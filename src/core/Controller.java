/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;
import javax.swing.JTextArea;
import lib.*;
import world.*;
/**
 *
 * @author jrwebster
 */
public class Controller {
    
    private Player player;
    private boolean game_over;
    private World world;
    private Tile current_tile;
    private BattleManager battle_manager;
    private EnemyGenerator enemy_generator;
    private Helper helper = new Helper();
    private PlayerCreator player_creator;
    private PlayerNavigation player_navigation;
    
    /**
     * 
     */
    public Controller() {
        
    }
    
    public void print_game_menu(JTextArea gameOutput) {
        String game_banner = helper.printBanner();
        gameOutput.append(game_banner);
    }
}
