/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;
import lib.Player;
import lib.Enemy;
import lib.Status;
import java.util.*;

/**
 *
 * @author jrwebster
 */
public class BattleManager {
    
    private Player player;
    private int xp_awarded;
    private int gold_awarded;
    private Controller controller;
    private Helper helper;
    private EnemyGenerator enemy_generator;
    static String sep = "=====================\n";
    
    /**
     * 
     */
    public BattleManager(Controller controller, Helper helper, Player player) {
        this.xp_awarded = 0;
        this.gold_awarded = 0;
        this.controller = controller;
        this.helper = helper;
        this.player = player;
    }
    
    /**
     * 
     * @return 
     */
    public boolean checkRandomBattle() {
        Random rand = new Random();
        // 40% chance = normal
        int battle_test = rand.nextInt(100 - 0 + 1);
        if(battle_test <= 40) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 
     * @param enemies
     * @return 
     */
    public boolean allEnemiesDead(List<Enemy> enemies) {
        boolean all_dead = false;
        for(int i = 0; i < enemies.size(); i++) {
            if(enemies.get(i).getStatus() == Status.DEAD) {
                all_dead = true;
            }
        }
        return all_dead;
    }
    
    /**
     * 
     */
    public void battle_menu() {
        
    }

    /**
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * @param player the player to set
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * @return the xp_awarded
     */
    public int getXp_awarded() {
        return xp_awarded;
    }

    /**
     * @param xp_awarded the xp_awarded to set
     */
    public void setXp_awarded(int xp_awarded) {
        this.xp_awarded = xp_awarded;
    }

    /**
     * @return the gold_awarded
     */
    public int getGold_awarded() {
        return gold_awarded;
    }

    /**
     * @param gold_awarded the gold_awarded to set
     */
    public void setGold_awarded(int gold_awarded) {
        this.gold_awarded = gold_awarded;
    }

    /**
     * @return the controller
     */
    public Controller getController() {
        return controller;
    }

    /**
     * @param controller the controller to set
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }
}
