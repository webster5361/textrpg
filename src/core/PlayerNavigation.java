/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 *
 * @author jrwebster
 */
public class PlayerNavigation {
    
    private int player_x;
    private int player_y;

    /**
     * @return the player_x
     */
    public int getPlayer_x() {
        return player_x;
    }

    /**
     * @param player_x the player_x to set
     */
    public void setPlayer_x(int player_x) {
        this.player_x = player_x;
    }

    /**
     * @return the player_y
     */
    public int getPlayer_y() {
        return player_y;
    }

    /**
     * @param player_y the player_y to set
     */
    public void setPlayer_y(int player_y) {
        this.player_y = player_y;
    }
    
    
}
