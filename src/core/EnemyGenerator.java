/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;
import java.util.*;
import lib.Enemy;
import lib.Player;

/**
 *
 * @author jrwebster
 */
public class EnemyGenerator {
    
    Controller controller;
    List<lib.Enemy> enemy_encyclopedia;
    
    /**
     * 
     */
    public EnemyGenerator() {
        
    }
    
    public int generate_random_age() {
        Random rand = new Random();
        int low = 10;
        int high = 85;
        int age = rand.nextInt(high - low) + low;
        return age;
    }
    
    public int generate_level(int player_level) {
        Random rand = new Random();
        int low;
        int high;
        if(player_level - 2 <= 0) {
            low = 1;
            high = player_level + 2;
        } else {
            low = player_level - 2;
            high = player_level + 2;
        }
        int level = rand.nextInt(high - low) + low;
        return level;
    }
    
    public Enemy generate_enemy(Player player) {
        Enemy temp_enemy = new Enemy();
        return temp_enemy;
    }
}
