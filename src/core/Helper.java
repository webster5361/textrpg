/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 *
 * @author jrwebster
 */
public class Helper {
    
    String banner_bar1 = "#                                                           #";
    String banner_bar2 = "#############################################################";
    String banner_bar3 = "#                          The Game                         #";
    
    public String printBanner() {
        return "\n" + banner_bar2 + banner_bar1 + banner_bar3 + banner_bar1 + banner_bar1 + banner_bar2 + "\n";
    }
    
    public String printMenu() {
        String new_game = "\n1.     New Game\n";
        String load_game = "2.     Load Game\n";
        String options = "3.     Options\n";
        String exit_game = "9.     Exit Game\n";
        return new_game + load_game + options + exit_game;
    }
}
