/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;
import lib.*;
import java.util.*;

/**
 *
 * @author jrwebster
 */
public class PlayerCreator {
    
    /**
     * 
     */
    public PlayerCreator() {
        
    }
    
    /**
     * 
     * @param name
     * @param age
     * @param pc 
     */
    public void CreatePlayer(String name, int age, PlayerClass pc) {
        Player new_player = new Player();
        switch(pc) {
            case FIGHTER:
                this.BuildFighter();
            case MAGE:
                this.BuildMage();
            case THIEF:
                this.BuildThief();
            case PRIEST:
                this.BuildPriest();
            default:
                break;
        }
    }
    
    /**
     * 
     */
    public void BuildFighter() {
        Player fighter = new Player();
        int strength = 16;
        int intelligence = 0;
        int stamina = 0;
        int speed = 0;
        
        this.CreateStartingEquipmentFighter();
    }
    
    /**
     * 
     */
    public void BuildMage() {
        Player mage = new Player();
        int strength = 10;
        int intelligence = 0;
        int stamina = 0;
        int speed = 0;
        
        this.CreateStartingEquipmentMage();
        this.CreateStartingSpellbookMage();
    }
    
    /**
     * 
     */
    public void BuildThief() {
        Player thief = new Player();
        int strength = 12;
        int intelligence = 0;
        int stamina = 0;
        int speed = 0;
        
        this.CreateStartingEquipmentThief();
    }
    
    /**
     * 
     */
    public void BuildPriest() {
        Player priest = new Player();
        int strength = 10;
        int intelligence = 0;
        int stamina = 0;
        int speed = 0;
        
        this.CreateStartingEquipmentPriest();
        this.CreateStartingSpellbookPriest();
    }
    
    /**
     * 
     */
    public void CreateStartingEquipmentFighter() {
        
    }
    
    /**
     * 
     */
    public void CreateStartingEquipmentMage() {
        
    }
    
    /**
     * 
     */
    public void CreateStartingEquipmentThief() {
        
    }
    
    /**
     * 
     */
    public void CreateStartingEquipmentPriest() {
        
    }
    
    /**
     * 
     */
    public void CreateStartingSpellbookMage() {
        
    }
    
    /**
     * 
     */
    public void CreateStartingSpellbookPriest() {
        
    }
}
