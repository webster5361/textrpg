/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;
import java.util.*;

/**
 *
 * @author jrwebster
 */
public class Tile {
    
    private int id;
    private String name;
    private int x;
    private int y;
    private boolean visited;
    private String description;
    private List<lib.Item> items;
    private List<lib.Enemy> enemies;
    private List<lib.NPC> npcs;
    private List<String> exits;
    private boolean is_locked;
    private String lock_message;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * @return the visited
     */
    public boolean isVisited() {
        return visited;
    }

    /**
     * @param visited the visited to set
     */
    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the items
     */
    public List<lib.Item> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<lib.Item> items) {
        this.items = items;
    }

    /**
     * @return the enemies
     */
    public List<lib.Enemy> getEnemies() {
        return enemies;
    }

    /**
     * @param enemies the enemies to set
     */
    public void setEnemies(List<lib.Enemy> enemies) {
        this.enemies = enemies;
    }

    /**
     * @return the npcs
     */
    public List<lib.NPC> getNpcs() {
        return npcs;
    }

    /**
     * @param npcs the npcs to set
     */
    public void setNpcs(List<lib.NPC> npcs) {
        this.npcs = npcs;
    }

    /**
     * @return the exits
     */
    public List<String> getExits() {
        return exits;
    }

    /**
     * @param exits the exits to set
     */
    public void setExits(List<String> exits) {
        this.exits = exits;
    }

    /**
     * @return the is_locked
     */
    public boolean isIs_locked() {
        return is_locked;
    }

    /**
     * @param is_locked the is_locked to set
     */
    public void setIs_locked(boolean is_locked) {
        this.is_locked = is_locked;
    }

    /**
     * @return the lock_message
     */
    public String getLock_message() {
        return lock_message;
    }

    /**
     * @param lock_message the lock_message to set
     */
    public void setLock_message(String lock_message) {
        this.lock_message = lock_message;
    }
    
}
