/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

/**
 *
 * @author jrwebster
 */
public class Weapon extends Item {
    
    private int attack;
    private float durability;
    private float max_durability;
    private EquipmentStatus status;

    /**
     * @return the attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * @param attack the attack to set
     */
    public void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * @return the durability
     */
    public float getDurability() {
        return durability;
    }

    /**
     * @param durability the durability to set
     */
    public void setDurability(float durability) {
        this.durability = durability;
    }

    /**
     * @return the max_durability
     */
    public float getMax_durability() {
        return max_durability;
    }

    /**
     * @param max_durability the max_durability to set
     */
    public void setMax_durability(float max_durability) {
        this.max_durability = max_durability;
    }

    /**
     * @return the status
     */
    public EquipmentStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(EquipmentStatus status) {
        this.status = status;
    }
    
    
}
