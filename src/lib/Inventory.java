/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;
import java.util.*;

/**
 *
 * @author jrwebster
 */
public class Inventory {
    
    private int gold;
    private List<Item> items = new ArrayList<Item>();

    public Inventory() {
        
    }
    
    /**
     * 
     * @return 
     */
    public List<String> getInventoryArray() {
        List<String> item_array = new ArrayList<>();
        for(int i = 0; i < items.size(); i++) {
            item_array.add(items.get(i).getName());
        }
        return item_array;
    }
    
    /**
     * 
     * @return 
     */
    public double getCarriedWeight() {
        double total_weight = 0;
        for(int i = 0; i < items.size(); i++) {
            total_weight += items.get(i).getWeight();
        }
        return total_weight;
    }
    
    /**
     * @return the gold
     */
    public int getGold() {
        return gold;
    }

    /**
     * @param gold the gold to set
     */
    public void setGold(int gold) {
        this.gold = gold;
    }

    /**
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }
    
    public void addItem(Item item) {
        this.items.add(item);
    }
    
    public void removeItem(Item item) {
      
    }
}
