/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

/**
 *
 * @author jrwebster
 */
public class Enemy extends Entity {

    private int strength;
    private int intelligence;
    private int stamina;
    private int speed;
    private int xp_dropped;
    private int attack;
    private int defense;
    private double attack_min;
    private double attack_max;
    private double defense_min;
    private double defense_max;

    public Enemy() {
        
    }
    
    /**
     * 
     * @return 
     */
    private int calculateXP_dropped() {
        return(2 * this.getLevel() * 10);
    }
    
    /**
     * 
     */
    private void levelUp() {
       this.setLevel(this.getLevel() + 1);
       this.setStrength(this.getStrength() + 1);
       this.setIntelligence(this.getIntelligence() + 1);
       this.setStamina(this.getStamina() + 1);
       if(this.getLevel() % 5 == 0) {
           this.setSpeed(this.getSpeed() + 1);
       }
    }
    
    private void calculateGoldToDrop() {
        
    }
    
    private void calculateXPtoDrop() {
        
    }
    
    /**
     * @return the strength
     */
    public int getStrength() {
        return strength;
    }

    /**
     * @param strength the strength to set
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }

    /**
     * @return the intelligence
     */
    public int getIntelligence() {
        return intelligence;
    }

    /**
     * @param intelligence the intelligence to set
     */
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    /**
     * @return the stamina
     */
    public int getStamina() {
        return stamina;
    }

    /**
     * @param stamina the stamina to set
     */
    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @return the xp_dropped
     */
    public int getXp_dropped() {
        return xp_dropped;
    }

    /**
     * @param xp_dropped the xp_dropped to set
     */
    public void setXp_dropped(int xp_dropped) {
        this.xp_dropped = xp_dropped;
    }

    /**
     * @return the attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * @param attack the attack to set
     */
    public void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * @return the defense
     */
    public int getDefense() {
        return defense;
    }

    /**
     * @param defense the defense to set
     */
    public void setDefense(int defense) {
        this.defense = defense;
    }

    /**
     * @return the attack_min
     */
    public double getAttack_min() {
        return attack_min;
    }

    /**
     * @param attack_min the attack_min to set
     */
    public void setAttack_min(double attack_min) {
        this.attack_min = attack_min;
    }

    /**
     * @return the attack_max
     */
    public double getAttack_max() {
        return attack_max;
    }

    /**
     * @param attack_max the attack_max to set
     */
    public void setAttack_max(double attack_max) {
        this.attack_max = attack_max;
    }

    /**
     * @return the defense_min
     */
    public double getDefense_min() {
        return defense_min;
    }

    /**
     * @param defense_min the defense_min to set
     */
    public void setDefense_min(double defense_min) {
        this.defense_min = defense_min;
    }

    /**
     * @return the defense_max
     */
    public double getDefense_max() {
        return defense_max;
    }

    /**
     * @param defense_max the defense_max to set
     */
    public void setDefense_max(double defense_max) {
        this.defense_max = defense_max;
    }
    
}
