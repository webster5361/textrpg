/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

/**
 *
 * @author jrwebster
 */
public class Entity {
    
    private int id;
    private int age;
    private int level;
    private int hp;
    private int max_hp;
    private int mp;
    private int max_mp;
    private String name;
    private Status status;
    Inventory backpack;

    /**
     * 
     * @param value 
     */
    public void reduce_health(int value) {
        if(this.getHp() - value <= 0) {
            this.setHp(0);
        } else {
            this.setHp(this.getHp() - value);
        }
    }
    
    /**
     * 
     * @param value 
     */
    public void increase_health(int value) {
        if(this.getHp() + value >= this.getMax_hp()) {
            this.setHp(this.getMax_hp());
        } else {
            this.setHp(this.getHp() + value);
        }
    }
    
    /**
     * 
     * @param value 
     */
    public void reduce_magic(int value) {
        if(this.getMp() - value <= 0) {
            this.setMp(0);
        } else {
            this.setMp(this.getMp() - value);
        }
    }
    
    /**
     * 
     * @param value 
     */
    public void increase_magic(int value) {
        if(this.getMp() + value >= this.getMax_mp()) {
            this.setMp(this.getMax_mp());
        } else {
            this.setMp(this.getMp() + value);
        }
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * @return the hp
     */
    public int getHp() {
        return hp;
    }

    /**
     * @param hp the hp to set
     */
    public void setHp(int hp) {
        this.hp = hp;
    }

    /**
     * @return the max_hp
     */
    public int getMax_hp() {
        return max_hp;
    }

    /**
     * @param max_hp the max_hp to set
     */
    public void setMax_hp(int max_hp) {
        this.max_hp = max_hp;
    }

    /**
     * @return the mp
     */
    public int getMp() {
        return mp;
    }

    /**
     * @param mp the mp to set
     */
    public void setMp(int mp) {
        this.mp = mp;
    }

    /**
     * @return the max_mp
     */
    public int getMax_mp() {
        return max_mp;
    }

    /**
     * @param max_mp the max_mp to set
     */
    public void setMax_mp(int max_mp) {
        this.max_mp = max_mp;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the backpack
     */
    public Inventory getBackpack() {
        return backpack;
    }

    /**
     * @param backpack the backpack to set
     */
    public void setBackpack(Inventory backpack) {
        this.backpack = backpack;
    }
    
}
