/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;
import java.util.*;

/**
 *
 * @author jrwebster
 */
public class Spellbook {
    
    private List<Spell> spells = new ArrayList<Spell>();

    /**
     * 
     */
    public Spellbook() {
        
    }
    
    /**
     * @return the spells
     */
    public List<Spell> getSpells() {
        return spells;
    }

    /**
     * @param spells the spells to set
     */
    public void setSpells(List<Spell> spells) {
        this.spells = spells;
    }
    
    /**
     * 
     * @param spell 
     */
    public void addSpell(Spell spell) {
        this.spells.add(spell);
    }
    
}
