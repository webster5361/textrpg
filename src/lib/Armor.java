/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

/**
 *
 * @author jrwebster
 */
public class Armor extends Item {
    
    private int defense;
    private float durability;
    private float max_durability;
    private EquipmentStatus status;
    
    /**
     * 
     * @param value 
     */
    public void damageArmor(int value) {
        if(this.getDurability() - value <= 0) {
            this.setDurability(0);
            this.evaluateArmor();
        } else {
            this.setDurability(this.getDurability() - value);
        }
    }
    
    /**
     * 
     * @param value 
     */
    public void repairArmor(int value) {
        if(this.getDurability() + value >= this.getMax_durability()) {
            this.setDurability(this.getMax_durability());
        } else {
            this.setDurability(this.getDurability() + value);
        }
    }
    
    /**
     * 
     */
    public void evaluateArmor() {
        if(this.getDurability() <= 0) {
            this.setStatus(EquipmentStatus.BROKEN);
        }
    }
    
    /**
     * 
     * @return 
     */
    public boolean checkIfBroken() {
        return this.getStatus() == EquipmentStatus.BROKEN;
    }
    
    /**
     * @return the defense
     */
    public int getDefense() {
        return defense;
    }

    /**
     * @param defense the defense to set
     */
    public void setDefense(int defense) {
        this.defense = defense;
    }

    /**
     * @return the durability
     */
    public float getDurability() {
        return durability;
    }

    /**
     * @param durability the durability to set
     */
    public void setDurability(float durability) {
        this.durability = durability;
    }

    /**
     * @return the max_durability
     */
    public float getMax_durability() {
        return max_durability;
    }

    /**
     * @param max_durability the max_durability to set
     */
    public void setMax_durability(float max_durability) {
        this.max_durability = max_durability;
    }

    /**
     * @return the status
     */
    public EquipmentStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(EquipmentStatus status) {
        this.status = status;
    }
    
}
