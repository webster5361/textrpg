/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

/**
 *
 * @author jrwebster
 */
public class Item {

    private String name;
    private String description;
    private int id;
    private int value;
    private double weight;
    private boolean usable;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * @return the usable
     */
    public boolean isUsable() {
        return usable;
    }

    /**
     * @param usable the usable to set
     */
    public void setUsable(boolean usable) {
        this.usable = usable;
    }
    
    @Override
    public String toString() {
        String print_str = String.format("\n%1$s\n%2$s lbs.\n%3$s gold\n%4$s\n", this.getName(), this.getWeight(), this.getValue(), this.getDescription());
        return print_str;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
}
