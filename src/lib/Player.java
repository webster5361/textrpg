/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;
import java.lang.Math;

/**
 *
 * @author jrwebster
 */
public class Player extends Entity {
    
    private int strength;
    private int intelligence;
    private int stamina;
    private int speed;
    private int stat_points;
    private int xp;
    private int xp_to_next_level;
    
    private Weapon equipped_weapon;
    private Armor equipped_armor;
    private Armor equipped_helmet;
    private Armor equipped_gloves;
    private Armor equipped_boots;
    
    private PlayerClass player_class;
    Spellbook spellbook;
    private boolean in_battle;
    
    private double carried_weight;
    private double max_carried_weight;

    /**
     * 
     */
    public Player() {
        this.equipped_weapon = new Weapon();
        this.equipped_armor = new Armor();
        this.equipped_helmet = new Armor();
        this.equipped_gloves = new Armor();
        this.equipped_boots = new Armor();
        this.backpack = new Inventory();
        this.spellbook = new Spellbook();
    }
    
    public void update_player_stats() {
        this.calculate_max_health();
        this.calculate_max_magic();
        this.calculate_next_level_xp(this.getLevel() + 1);
    }
    
    /**
     * 
     */
    public void check_level_up() {
        if(this.xp == this.xp_to_next_level) {
            this.level_up();
        }
    }
    
    /**
     * 
     */
    private void level_up() {
        this.setLevel(this.getLevel()+1);
        this.calculate_next_level_xp(this.getLevel()+1);
        this.calculate_max_health();
        this.setHp(this.getMax_hp());
        this.calculate_max_magic();
        this.setMp(this.getMax_mp());
        this.setStat_points(this.getStat_points()+3);
    }
    
    /**
     * 
     */
    private void calculate_max_health() {
        double new_max_health = ((this.getStamina() + (this.getStrength() / 2) * this.getLevel()) * 5.5);
        this.setMax_hp((int)new_max_health);
    }
    
    /**
     * 
     */
    private void calculate_max_magic() {
        double new_max_magic = ((this.getIntelligence() + (this.getStamina() / 2) * this.getLevel()) * 3);
        this.setMax_mp((int)new_max_magic);
    }
    
    /**
     * 
     * @param level 
     */
    private void calculate_next_level_xp(int level) {
        double new_xp_to_next_level = 50 * Math.pow((this.getLevel()+1), 2) - (50 * (this.getLevel()+1));
        this.xp_to_next_level = (int)new_xp_to_next_level;
    }
    
    /**
     * 
     * @return 
     */
    public int calculate_attack_power() {
        int attack_power = 0;
        return attack_power;
    }
    
    /**
     * 
     * @return 
     */
    public int calculate_magic_attack_power() {
        int magic_attack_power = 0;
        return magic_attack_power;
    }
    
    /**
     * 
     * @return 
     */
    public int calculate_armor_rating() {
        int armor_rating = 0;
        return armor_rating;
    }
    
    /**
     * 
     * @param stat_to_increase
     * @param number_of_points
     * @return 
     */
    public boolean add_stat_point(String stat_to_increase, int number_of_points) {
        return false;
    }
    
    /**
     * @return the strength
     */
    public int getStrength() {
        return strength;
    }

    /**
     * @param strength the strength to set
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }

    /**
     * @return the intelligence
     */
    public int getIntelligence() {
        return intelligence;
    }

    /**
     * @param intelligence the intelligence to set
     */
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    /**
     * @return the stamina
     */
    public int getStamina() {
        return stamina;
    }

    /**
     * @param stamina the stamina to set
     */
    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @return the stat_points
     */
    public int getStat_points() {
        return stat_points;
    }

    /**
     * @param stat_points the stat_points to set
     */
    public void setStat_points(int stat_points) {
        this.stat_points = stat_points;
    }

    /**
     * @return the xp
     */
    public int getXp() {
        return xp;
    }

    /**
     * @param xp the xp to set
     */
    public void setXp(int xp) {
        this.xp = xp;
    }

    /**
     * @return the xp_to_next_level
     */
    public int getXp_to_next_level() {
        return xp_to_next_level;
    }

    /**
     * @param xp_to_next_level the xp_to_next_level to set
     */
    public void setXp_to_next_level(int xp_to_next_level) {
        this.xp_to_next_level = xp_to_next_level;
    }

    /**
     * @return the equipped_weapon
     */
    public Weapon getEquipped_weapon() {
        return equipped_weapon;
    }

    /**
     * @param equipped_weapon the equipped_weapon to set
     */
    public void setEquipped_weapon(Weapon equipped_weapon) {
        this.equipped_weapon = equipped_weapon;
    }

    /**
     * @return the equipped_armor
     */
    public Armor getEquipped_armor() {
        return equipped_armor;
    }

    /**
     * @param equipped_armor the equipped_armor to set
     */
    public void setEquipped_armor(Armor equipped_armor) {
        this.equipped_armor = equipped_armor;
    }

    /**
     * @return the equipped_helmet
     */
    public Armor getEquipped_helmet() {
        return equipped_helmet;
    }

    /**
     * @param equipped_helmet the equipped_helmet to set
     */
    public void setEquipped_helmet(Armor equipped_helmet) {
        this.equipped_helmet = equipped_helmet;
    }

    /**
     * @return the equipped_gloves
     */
    public Armor getEquipped_gloves() {
        return equipped_gloves;
    }

    /**
     * @param equipped_gloves the equipped_gloves to set
     */
    public void setEquipped_gloves(Armor equipped_gloves) {
        this.equipped_gloves = equipped_gloves;
    }

    /**
     * @return the equipped_boots
     */
    public Armor getEquipped_boots() {
        return equipped_boots;
    }

    /**
     * @param equipped_boots the equipped_boots to set
     */
    public void setEquipped_boots(Armor equipped_boots) {
        this.equipped_boots = equipped_boots;
    }

    /**
     * @return the spellbook
     */
    public Spellbook getSpellbook() {
        return spellbook;
    }

    /**
     * @param spellbook the spellbook to set
     */
    public void setSpellbook(Spellbook spellbook) {
        this.spellbook = spellbook;
    }

    /**
     * @return the in_battle
     */
    public boolean isIn_battle() {
        return in_battle;
    }

    /**
     * @param in_battle the in_battle to set
     */
    public void setIn_battle(boolean in_battle) {
        this.in_battle = in_battle;
    }

    /**
     * @return the player_class
     */
    public PlayerClass getPlayer_class() {
        return player_class;
    }

    /**
     * @param player_class the player_class to set
     */
    public void setPlayer_class(PlayerClass player_class) {
        this.player_class = player_class;
    }

    /**
     * @return the carried_weight
     */
    public double getCarried_weight() {
        return carried_weight;
    }

    /**
     * @param carried_weight the carried_weight to set
     */
    public void setCarried_weight(double carried_weight) {
        this.carried_weight = carried_weight;
    }

    /**
     * @return the max_carried_weight
     */
    public double getMax_carried_weight() {
        return max_carried_weight;
    }

    /**
     * @param max_carried_weight the max_carried_weight to set
     */
    public void setMax_carried_weight(double max_carried_weight) {
        this.max_carried_weight = max_carried_weight;
    }
    
}
